package com.example.currencysecondv;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.currencysecondv.exception.SurviveDialog;
import com.example.currencysecondv.listeners.EditTextListeners;
import com.example.currencysecondv.service.AllDataService;

public class CalculateActivity extends AppCompatActivity implements View.OnClickListener {

    private Button premiumAllBtn;
    private TextView fromNameCode;
    private TextView toNameCode;
    private EditText etUnit;
    private EditText etCourse;
    private Intent changeCurrency;

    private String firstCurrencyCode, secondCurrencyCode;
    private String currencyUnit;
    private String firstCurrencyName, secondCurrencyName;
    private String currencyCourse;
    private String totalFirstValue, totalSecondValue;
    private String typeChecker;

    private final String KEY_ALL_CODE = "CODE";
    private final String KEY_ALL_UNIT = "UNIT";
    private final String KEY_ALL_NAME = "NAME";
    private final String KEY_ALL_COURSE = "COURSE";
    private final String KEY_TYPE = "TYPE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculate);

        getInfo();
        init();
        action();
        listener();
    }

    private void getInfo() {
        Bundle getIntentDate = getIntent().getExtras();
        typeChecker = getIntentDate.getString(KEY_TYPE);
        if (typeChecker == null) {
            firstCurrencyCode = getIntent().getStringExtra("CODE");
            currencyUnit = getIntent().getStringExtra("UNIT");
            firstCurrencyName = getIntent().getStringExtra("NAME");
            currencyCourse = getIntent().getStringExtra("COURSE");
            secondCurrencyCode = getIntent().getStringExtra("RUB");
            secondCurrencyName = getIntent().getStringExtra("RUBEL");

        } else if (typeChecker.equals("FIRST")) {
            totalFirstValue = getIntentDate.getString("S");

        } else if (typeChecker.equals("SECOND")) {
            totalFirstValue = getIntentDate.getString("DEFAULT");
            totalSecondValue = getIntentDate.getString("S");
        }
    }

    @SuppressLint("SetTextI18n")
    private void action() {
        if (typeChecker != null)
            if (typeChecker.equals("FIRST")) {
                String[] value = totalFirstValue.split(",");
                currencyCourse = value[3];
                currencyUnit = value[2];
                firstCurrencyName = value[1];
                firstCurrencyCode = value[0];
                secondCurrencyName = "Рубль";
                secondCurrencyCode = "RUB";

            } else if (typeChecker.equals("SECOND")) {
                String[] value = totalFirstValue.split(",");
                String[] value2 = totalSecondValue.split(",");
                currencyCourse = value[2];
                currencyUnit = value2[2];
                firstCurrencyName = value[1];
                secondCurrencyName = value2[1];
                firstCurrencyCode = value[0];
                secondCurrencyCode = value2[0];
            }
        fromNameCode.setText(firstCurrencyCode + "\n (" + firstCurrencyName + ")");
        toNameCode.setText(secondCurrencyCode + "\n (" + secondCurrencyName + ")");
        etUnit.setText(currencyUnit);
        etCourse.setText(currencyCourse);
    }

    private void init() {
        premiumAllBtn = findViewById(R.id.oneTwoMany);
        fromNameCode = findViewById(R.id.from);
        toNameCode = findViewById(R.id.to);
        etUnit = findViewById(R.id.currency_to_be_converted);
        etCourse = findViewById(R.id.currency_converted);
        changeCurrency = new Intent(getApplicationContext(), ChooseCurrencyActivity.class);
    }

    private void listener() {
        premiumAllBtn.setOnClickListener(this);
        fromNameCode.setOnClickListener(this);
        toNameCode.setOnClickListener(this);
        etCourse.setOnClickListener(this);

        etCourse.addTextChangedListener(new EditTextListeners(
                new AllDataService(this, currencyCourse, currencyUnit), etUnit, 2));
        etUnit.addTextChangedListener(new EditTextListeners(
                new AllDataService(this, currencyCourse, currencyUnit), etCourse, 1));
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.to:
                changeCurrency.putExtra(KEY_ALL_CODE, firstCurrencyCode);
                changeCurrency.putExtra(KEY_ALL_UNIT, currencyUnit);
                changeCurrency.putExtra(KEY_ALL_NAME, firstCurrencyName);
                changeCurrency.putExtra(KEY_ALL_COURSE, currencyCourse);
                changeCurrency.putExtra(KEY_TYPE, "SECOND");
                startActivity(changeCurrency);
                break;
            case R.id.from:
                changeCurrency.putExtra(KEY_TYPE, "FIRST");
                startActivity(changeCurrency);
                break;
            case R.id.currency_converted:
            case R.id.oneTwoMany:
                startActivity(new Intent(this, OneTwoManyCalculateActivity.class));
                break;
            default:
                new SurviveDialog("Огоооо", "Кааааак?").show(getSupportFragmentManager(), "double");
                break;
        }
    }
}