package com.example.currencysecondv;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import com.example.currencysecondv.exception.SurviveDialog;
import com.example.currencysecondv.listeners.ItemClickListener;
import com.example.currencysecondv.model.ModelCurrency;
import com.example.currencysecondv.parse.HTMLParser;
import com.example.currencysecondv.view.ChangeCurrencyAdapter;

import org.jsoup.nodes.Document;

import java.util.ArrayList;

public class ChooseCurrencyActivity extends AppCompatActivity {
    private RecyclerView recyclerAllInfo;
    private ChangeCurrencyAdapter changeCurrencyAdapter;
    private Intent previousPage;

    private ItemClickListener radioButtonListener;

    private ArrayList<ModelCurrency> allInfoData;
    private HTMLParser htmlParser;

    private final String KEY_TYPE = "TYPE";
    private final String KEY_VALUE = "S";
    public final String KEY_DEFAULT = "DEFAULT";

    private String currencyCode;
    private String currencyUnit;
    private String currencyName;
    private String currencyCourse;
    private String typeConvert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_currency);

        getInfo();
        init();
        action();
        listener();
        buildRecyclerView();
    }

    private void buildRecyclerView() {
        recyclerAllInfo.setLayoutManager(new LinearLayoutManager(this));
        changeCurrencyAdapter = new ChangeCurrencyAdapter(allInfoData, radioButtonListener, currencyUnit, currencyCourse, typeConvert);
        recyclerAllInfo.setAdapter(changeCurrencyAdapter);
    }

    private void getInfo() {
        currencyCode = getIntent().getStringExtra("CODE");
        currencyUnit = getIntent().getStringExtra("UNIT");
        currencyName = getIntent().getStringExtra("NAME");
        currencyCourse = getIntent().getStringExtra("COURSE");
        typeConvert = getIntent().getStringExtra("TYPE");
    }

    public void init() {
        recyclerAllInfo = findViewById(R.id.chooseRecycler);
        allInfoData = new ArrayList<>();
        htmlParser = new HTMLParser("http://www.cbr.ru/currency_base/daily/");
        previousPage = new Intent(this, CalculateActivity.class);
    }

    private void action() {
        @SuppressLint("NotifyDataSetChanged") Runnable parse = () -> {
            Document document = htmlParser.parse();
            if (document == null) {
                new SurviveDialog("", "Извините, у нас проблема с парсингом, переподключайтесь").show(getSupportFragmentManager(), "alertdialog_single");
            } else {
                allInfoData.addAll(htmlParser.getTable(document));
                runOnUiThread(() -> this.changeCurrencyAdapter.notifyDataSetChanged());
            }
        };
        Thread third = new Thread(parse);
        third.start();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void listener() {
        radioButtonListener = s -> {
            recyclerAllInfo.post(() -> changeCurrencyAdapter.notifyDataSetChanged());

            Intent calculatePage = new Intent(this, CalculateActivity.class);
            calculatePage.putExtra(KEY_TYPE, typeConvert);
            if (typeConvert.equals("FIRST")) {
                calculatePage.putExtra(KEY_VALUE, s);
            } else {
                calculatePage.putExtra(KEY_VALUE, s);
                calculatePage.putExtra(KEY_DEFAULT, currencyCode + "," + currencyName + "," + currencyUnit);
            }
            startActivity(calculatePage);
        };
    }

}