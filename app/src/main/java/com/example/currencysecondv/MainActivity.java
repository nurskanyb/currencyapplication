package com.example.currencysecondv;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.currencysecondv.exception.SurviveDialog;
import com.example.currencysecondv.listeners.EditTextListeners;
import com.example.currencysecondv.listeners.RefreshListener;
import com.example.currencysecondv.model.ModelCurrency;
import com.example.currencysecondv.parse.HTMLParser;
import com.example.currencysecondv.service.AllDataService;
import com.example.currencysecondv.view.AllInfoAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.jsoup.nodes.Document;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private RecyclerView recyclerAllInfo;
    private AllInfoAdapter allInfoAdapter;
    private SwipeRefreshLayout refreshLayout;

    private ArrayList<ModelCurrency> allInfoData;
    private HTMLParser htmlParser;

    private FloatingActionButton hideSearch;
    private EditText searchCurrency;
    private AdView advertisement;

    private boolean searchChecker = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (internetChecker()) {
            init();
            buildRecyclerView();
            actions();
            listeners();
            advertisement();
        } else {
            new SurviveDialog("", getString(R.string.internet_con)).show(getSupportFragmentManager(), "alertdialog_single");
        }
    }

    private boolean internetChecker() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED;
    }


    public void init() {
        recyclerAllInfo = findViewById(R.id.recyclerAllInfo);
        hideSearch = (FloatingActionButton) findViewById(R.id.hideSearch);
        searchCurrency = findViewById(R.id.searchCurrency);
        refreshLayout = findViewById(R.id.refreshLayout);

        htmlParser = new HTMLParser("http://www.cbr.ru/currency_base/daily/");
        allInfoData = new ArrayList<>();
        allInfoAdapter = new AllInfoAdapter(allInfoData, MainActivity.this);
    }

    private void buildRecyclerView() {
        LinearLayoutManager searchedInfo = new LinearLayoutManager(this);
        recyclerAllInfo.setHasFixedSize(true);
        recyclerAllInfo.setLayoutManager(searchedInfo);
        recyclerAllInfo.setAdapter(allInfoAdapter);
    }

    @SuppressLint("NotifyDataSetChanged")
    private void actions() {
        Runnable parse = () -> {
            Document document = htmlParser.parse();
            if (document == null) {
                new SurviveDialog("", getString(R.string.parsing_toast)).show(getSupportFragmentManager(), "alertdialog_single");
            } else {
                allInfoData.addAll(htmlParser.getTable(document));
                runOnUiThread(() -> this.allInfoAdapter.notifyDataSetChanged());
            }
        };
        Thread secondTh = new Thread(parse);
        secondTh.start();
    }

    private void listeners() {
        hideSearch.setOnClickListener(this);
        searchCurrency.addTextChangedListener(new EditTextListeners(new AllDataService(this, allInfoData, allInfoAdapter)));
        refreshLayout.setOnRefreshListener(new RefreshListener(this, refreshLayout));
    }

    private void advertisement() {
        MobileAds.initialize(this, initializationStatus -> System.out.println("Done"));
        advertisement = findViewById(R.id.advertisement);
        AdRequest adRequest = new AdRequest.Builder().build();
        advertisement.loadAd(adRequest);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.hideSearch:
                if (searchChecker) {
                    searchCurrency.setVisibility(View.VISIBLE);
                    searchChecker = false;
                } else {
                    searchCurrency.setVisibility(View.GONE);
                    searchChecker = true;
                }
                break;
            default:
                new SurviveDialog("Огоо", "как ты смог").show(getSupportFragmentManager(), "alertdialog_double");
                break;
        }
    }
}