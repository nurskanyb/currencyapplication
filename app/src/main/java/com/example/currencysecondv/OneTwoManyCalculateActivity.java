package com.example.currencysecondv;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.os.Bundle;

import com.example.currencysecondv.exception.SurviveDialog;
import com.example.currencysecondv.model.ModelCurrency;
import com.example.currencysecondv.parse.HTMLParser;
import com.example.currencysecondv.view.PremiumAllInfoAd;

import org.jsoup.nodes.Document;

import java.util.ArrayList;
/**/
public class OneTwoManyCalculateActivity extends AppCompatActivity {
    private RecyclerView pRecyclerViewFirst;
    private RecyclerView pRecyclerViewSecond;
    private PremiumAllInfoAd premiumAllInfoAd;

    private ArrayList<ModelCurrency> allInfoData;
    private HTMLParser htmlParser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_two_many_calculate);
        init();
        actions();
        listeners();
        buildRecyclerView();
    }

    private void init() {
        pRecyclerViewFirst = findViewById(R.id.pRecyclerViewFirst);
        pRecyclerViewSecond = findViewById(R.id.pRecyclerViewSecond);

        htmlParser = new HTMLParser("http://www.cbr.ru/currency_base/daily/");
        allInfoData = new ArrayList<>();

        premiumAllInfoAd = new PremiumAllInfoAd(allInfoData, this);
    }

    public void buildRecyclerView() {
        LinearLayoutManager searchedInfoFirst = new LinearLayoutManager(this);
        pRecyclerViewFirst.setHasFixedSize(true);
        pRecyclerViewFirst.setLayoutManager(searchedInfoFirst);
        pRecyclerViewFirst.setAdapter(premiumAllInfoAd);

        LinearLayoutManager searchedInfoSecond = new LinearLayoutManager(this);
        pRecyclerViewSecond.setHasFixedSize(true);
        pRecyclerViewSecond.setLayoutManager(searchedInfoSecond);
        pRecyclerViewSecond.setAdapter(premiumAllInfoAd);
    }

    public void actions() {
        @SuppressLint("NotifyDataSetChanged") Runnable parse = () -> {
            Document document = htmlParser.parse();
            if (document == null) {
                new SurviveDialog("", getString(R.string.parsing_toast)).show(getSupportFragmentManager(), "alertdialog_single");
            } else {
                allInfoData.addAll(htmlParser.getTable(document));
                runOnUiThread(() -> this.premiumAllInfoAd.notifyDataSetChanged());
            }
        };
        Thread secondTh = new Thread(parse);
        secondTh.start();
    }

    private void listeners() {
    }

}