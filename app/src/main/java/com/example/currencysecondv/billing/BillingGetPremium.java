package com.example.currencysecondv.billing;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.PurchaseInfo;

public class BillingGetPremium implements BillingProcessor.IBillingHandler {

    private BillingProcessor bp;
    private Context context;
    private final String KEY = "YOUR LICENSE KEY FROM GOOGLE PLAY CONSOLE HERE";

    public BillingGetPremium(Context context, BillingProcessor bp) {
        this.context = context;
        this.bp = bp;
    }

    public void initial() {
        bp = new BillingProcessor(context, "YOUR LICENSE KEY FROM GOOGLE PLAY CONSOLE HERE", this);
        bp.initialize();
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable PurchaseInfo details) {
        Log.d("activity", "onProductPurchased");
    }

    @Override
    public void onPurchaseHistoryRestored() {
        Log.d("activity", "onPurchaseHistoryRestored");
    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {
        Log.d("activity", "onBillingError");
    }

    @Override
    public void onBillingInitialized() {
        Log.d("activity", "onBillingInitialized");
    }
}
