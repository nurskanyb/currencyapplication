package com.example.currencysecondv.exception;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.example.currencysecondv.R;
//тут вся диалоговые окно
public class SurviveDialog extends DialogFragment implements DialogInterface.OnClickListener {
    private final String title;
    private final String message;

    public SurviveDialog(String title, String message) {
        this.title = title;
        this.message = message;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (title.equals("")) return (builder.setMessage(message)
                .setPositiveButton(R.string.okey, this)
                .create());
        else
            return (builder.setTitle(title).setMessage(message)
                    .setPositiveButton(R.string.retry, this)
                    .setNegativeButton(R.string.exit, this)
                    .create());
    }

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        super.onCancel(dialog);
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        requireActivity().finish();
        startActivity(requireActivity().getIntent());
        super.onDismiss(dialog);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        Toast.makeText(getActivity(), R.string.refreshed, Toast.LENGTH_LONG).show();
    }
}
