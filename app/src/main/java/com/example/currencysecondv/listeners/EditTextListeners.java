package com.example.currencysecondv.listeners;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.example.currencysecondv.service.AllDataService;

//обработка данных при вводе
public class EditTextListeners implements TextWatcher {
    private AllDataService allDataService;
    private EditText secondEdit;
    private boolean editTextChecker = true;
    private int type = 0;

    public EditTextListeners(AllDataService allDataService) {
        this.allDataService = allDataService;
    }

    public EditTextListeners(AllDataService allDataService, EditText editText, int type) {
        this.allDataService = allDataService;
        this.secondEdit = editText;
        this.type = type;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (!charSequence.toString().equals(""))
            if (type == 0)
                allDataService.findInfo(charSequence.toString());
            else if (type == 1) {
                if (editTextChecker) {
                    editTextChecker = false;
                    secondEdit.setText(allDataService.calculateByCourse(charSequence.toString(), 1));
                } else {
                    editTextChecker = true;
                }
            } else if (type == 2) {
                if (editTextChecker) {
                    secondEdit.setText(allDataService.calculateByCourse(charSequence.toString(), 2));
                    editTextChecker = false;
                } else {
                    editTextChecker = true;
                }
            }
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

}
