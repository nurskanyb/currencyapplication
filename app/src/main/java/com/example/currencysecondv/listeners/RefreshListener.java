package com.example.currencysecondv.listeners;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

//Обновление страниц
public class RefreshListener implements SwipeRefreshLayout.OnRefreshListener {
    private Activity page;
    private SwipeRefreshLayout refreshLayout;

    public RefreshListener(Activity context, SwipeRefreshLayout refreshLayout) {
        page = context;
        this.refreshLayout = refreshLayout;
    }

    @Override
    public void onRefresh() {
        Toast.makeText(page, "Обновлено", Toast.LENGTH_SHORT).show();

        page.finish();
        page.startActivity(page.getIntent());
        refreshLayout.setRefreshing(false);
    }
}
