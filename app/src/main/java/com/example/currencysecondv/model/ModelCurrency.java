package com.example.currencysecondv.model;

//обработка данных
public class ModelCurrency {
    private String currencyCode;
    private String currencyUnit;
    private String currencyName;
    private String currencyCourse;

    public ModelCurrency(String currencyCode, String currencyUnit, String currencyName, String currencyCourse) {
        this.currencyCode = currencyCode;
        this.currencyUnit = currencyUnit;
        this.currencyName = currencyName;
        this.currencyCourse = currencyCourse;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyUnit() {
        return currencyUnit;
    }

    public void setCurrencyUnit(String currencyUnit) {
        this.currencyUnit = currencyUnit;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getCurrencyCourse() {
        return currencyCourse;
    }

    public void setCurrencyCourse(String currencyCourse) {
        this.currencyCourse = currencyCourse;
    }
}
