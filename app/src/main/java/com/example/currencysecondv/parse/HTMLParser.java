package com.example.currencysecondv.parse;

import com.example.currencysecondv.model.ModelCurrency;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

//парсинг html
public class HTMLParser {
    private final String link;
    List<ModelCurrency> parsedDate = new ArrayList<>();

    public HTMLParser(String link) {
        this.link = link;
    }

    public Document parse() {
        Document parsedDocument;
        try {
            parsedDocument = Jsoup.connect(link).get();
        } catch (IOException e) {
            return null;
        }
        return parsedDocument;
    }

    public List<ModelCurrency> getTable(Document parsedDocument) {
        Elements currencyTables = Objects.requireNonNull(parsedDocument).getElementsByTag("tbody");
        Element table = currencyTables.get(0);

        for (int i = 0; i < table.childrenSize(); i++) {
            ModelCurrency listCurrency = new ModelCurrency(
                    table.children().get(i).child(1).text(),
                    table.children().get(i).child(2).text(),
                    table.children().get(i).child(3).text(),
                    table.children().get(i).child(4).text());
            parsedDate.add(listCurrency);
        }
        return parsedDate;
    }

}
