package com.example.currencysecondv.service;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.Toast;

import com.example.currencysecondv.model.ModelCurrency;
import com.example.currencysecondv.view.AllInfoAdapter;

import java.util.ArrayList;
import java.util.List;
//класс для работы всями алгоритмами текста
public class AllDataService {
    private ArrayList<ModelCurrency> infoData;
    private AllInfoAdapter infoAdapter;
    private Context context;
    private String course;
    private String unit;

    public AllDataService(Context context, ArrayList<ModelCurrency> allInfoData, AllInfoAdapter allInfoAdapter) {
        infoAdapter = allInfoAdapter;
        infoData = allInfoData;
        this.context = context;
    }

    public AllDataService(Context context, String course, String unit) {
        this.context = context;
        this.course = course;
        this.unit = unit;
    }

    public AllDataService() {
    }

    //ищем текст из list
    @SuppressLint("NotifyDataSetChanged")
    public void findInfo(String sentence) {
        ArrayList<ModelCurrency> filteredList = new ArrayList<>();

        for (ModelCurrency item : infoData) {
            if (item.getCurrencyCode().toLowerCase().contains(sentence.toLowerCase())) {
                filteredList.add(item);
            }
        }
        if (filteredList.isEmpty()) {
            Toast.makeText(context, "Таких нету", Toast.LENGTH_SHORT).show();

        } else {
            infoAdapter.filterList(filteredList);
        }
    }

    public String calculateByCourse(String toString, int type) {
        double mean = Double.parseDouble(toString.replace(',', '.'));
        double firstValue = Double.parseDouble(course.replace(',', '.'));
        double secondValue = Double.parseDouble(unit.replace(',', '.'));
        if (type == 1)
            return String.valueOf((firstValue * mean) / secondValue);
        else
            return String.valueOf((secondValue * mean) / firstValue);
    }

    public String calculator(String c, String u, ModelCurrency allData, String type) {
        if (type.equals("FIRST")) {
            return allData.getCurrencyCode() + "," +
                    allData.getCurrencyName() + "," +
                    allData.getCurrencyUnit() + "," +
                    allData.getCurrencyCourse();
        } else {
            double unit = Double.parseDouble(u.replace(',', '.'));
            double course = Double.parseDouble(c.replace(',', '.'));

            return allData.getCurrencyCode() + "," +
                    allData.getCurrencyName() + "," +
                    (Double.parseDouble(allData.getCurrencyCourse().replace(',', '.')) * unit) / course;
        }
    }
}
