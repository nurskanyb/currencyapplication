package com.example.currencysecondv.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.currencysecondv.CalculateActivity;
import com.example.currencysecondv.R;
import com.example.currencysecondv.model.ModelCurrency;

import java.util.ArrayList;

//экран для главного экран
public class AllInfoAdapter extends RecyclerView.Adapter<AllInfoAdapter.ViewHolder> {
    private ArrayList<ModelCurrency> allInfo;
    private Context context;
    private Intent newPageIn;

    private final String KEY_ALL_CODE = "CODE";
    private final String KEY_ALL_UNIT = "UNIT";
    private final String KEY_ALL_NAME = "NAME";
    private final String KEY_ALL_COURSE = "COURSE";

    public AllInfoAdapter(ArrayList<ModelCurrency> allInfo, Context context) {
        this.allInfo = allInfo;
        this.context = context;
    }

    @SuppressLint("NotifyDataSetChanged")
    public void filterList(ArrayList<ModelCurrency> filterList) {
        allInfo = filterList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AllInfoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View getPage = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_all_info, parent, false);
        return new ViewHolder(getPage);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull AllInfoAdapter.ViewHolder holder, int position) {
        ModelCurrency actionPage = allInfo.get(position);
        holder.firstCurrencyCode.setText(actionPage.getCurrencyCode());
        holder.firstCurrencyName.setText(actionPage.getCurrencyName());
        holder.firstCurrencyUnit.setText(actionPage.getCurrencyUnit());
        holder.secondCurrencyCode.setText("RUB");
        holder.currencyCourse.setText(actionPage.getCurrencyCourse());
        holder.secondCurrencyName.setText("Рубль");
        holder.newPage.setOnClickListener(view -> {

            newPageIn = new Intent(context, CalculateActivity.class);
            newPageIn.putExtra(KEY_ALL_CODE, actionPage.getCurrencyCode());
            newPageIn.putExtra(KEY_ALL_UNIT, actionPage.getCurrencyUnit());
            newPageIn.putExtra(KEY_ALL_NAME, actionPage.getCurrencyName());
            newPageIn.putExtra(KEY_ALL_COURSE, actionPage.getCurrencyCourse());
            newPageIn.putExtra("RUB", "RUB");
            newPageIn.putExtra("RUBEL", "Рубль");
            context.startActivity(newPageIn);
        });

    }

    @Override
    public int getItemCount() {
        return allInfo.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView firstCurrencyCode;
        private TextView firstCurrencyName;
        private TextView firstCurrencyUnit;
        private TextView secondCurrencyCode;
        private TextView currencyCourse;
        private TextView secondCurrencyName;
        private ConstraintLayout newPage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            firstCurrencyCode = itemView.findViewById(R.id.pCurrencyCode);
            firstCurrencyName = itemView.findViewById(R.id.pCurrencyName);
            firstCurrencyUnit = itemView.findViewById(R.id.pCurrencyUnit);
            secondCurrencyCode = itemView.findViewById(R.id.secondCurrencyCode);
            currencyCourse = itemView.findViewById(R.id.currencyCourse);
            secondCurrencyName = itemView.findViewById(R.id.secondCurrencyName);
            newPage = itemView.findViewById(R.id.newPage);
        }
    }
}
