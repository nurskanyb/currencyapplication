package com.example.currencysecondv.view;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.currencysecondv.R;
import com.example.currencysecondv.listeners.ItemClickListener;
import com.example.currencysecondv.model.ModelCurrency;
import com.example.currencysecondv.service.AllDataService;

import java.util.List;

public class ChangeCurrencyAdapter extends RecyclerView.Adapter<ChangeCurrencyAdapter.ViewHolder> {

    private List<ModelCurrency> allData;
    private ItemClickListener radioTextListener;
    private AllDataService allDataService = new AllDataService();

    private int selectedPosition = -1;

    private String unit;
    private String course;
    private String type;

//    экран для изменение данных
    public ChangeCurrencyAdapter(List<ModelCurrency> arrayList, ItemClickListener itemClickListener, String unit, String course, String type) {
        allData = arrayList;
        radioTextListener = itemClickListener;

        this.unit = unit;
        this.course = course;
        this.type = type;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View listViewPage = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_choose, parent, false);
        return new ViewHolder(listViewPage);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ModelCurrency controlModel = allData.get(position);

        holder.radioButton.setText(controlModel.getCurrencyCode() + ": " + controlModel.getCurrencyName());
        holder.radioButton.setChecked(position == selectedPosition);
        holder.radioButton.setOnCheckedChangeListener(
                (compoundButton, b) -> {
                    if (b) {
                        selectedPosition = holder.getAdapterPosition();
                        radioTextListener.onClick(allDataService.calculator(course, unit, controlModel, type));
                    }
                });
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return allData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        RadioButton radioButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            radioButton = itemView.findViewById(R.id.select);
        }
    }
}
