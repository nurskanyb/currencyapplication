package com.example.currencysecondv.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.currencysecondv.R;
import com.example.currencysecondv.model.ModelCurrency;

import java.util.ArrayList;

//экран для премиум страниц
public class PremiumAllInfoAd extends RecyclerView.Adapter<PremiumAllInfoAd.ViewHolder> {
    private ArrayList<ModelCurrency> allInfo;
    private Context context;

    public PremiumAllInfoAd(ArrayList<ModelCurrency> allInfo, Context context) {
        this.allInfo = allInfo;
        this.context = context;
    }

    @NonNull
    @Override
    public PremiumAllInfoAd.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View getPage = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_calculate, parent, false);
        return new ViewHolder(getPage);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ModelCurrency modelCurrency = allInfo.get(position);

        holder.pCurrencyCode.setText(modelCurrency.getCurrencyCode());
        holder.pCurrencyName.setText(modelCurrency.getCurrencyName());
        holder.pCurrencyUnit.setText(modelCurrency.getCurrencyUnit());
    }

    @Override
    public int getItemCount() {
        return allInfo.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private CheckBox chooseMany;
        private TextView pCurrencyCode;
        private TextView pCurrencyName;
        private TextView pCalculatedAns;
        private EditText pCurrencyUnit;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            chooseMany = itemView.findViewById(R.id.chooseMany);
            pCurrencyCode = itemView.findViewById(R.id.pCurrencyCode);
            pCurrencyName = itemView.findViewById(R.id.pCurrencyName);
            pCalculatedAns = itemView.findViewById(R.id.pCalculatedAns);
            pCurrencyUnit = itemView.findViewById(R.id.pCurrencyUnit);
        }
    }
}
